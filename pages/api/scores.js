const axios = require('axios')
const HTMLParser = require('node-html-parser')


export default async (req, res) => {
  const scoresHtml = await getHtmlScoresPage()
  const result = await getDataFromHTML(scoresHtml)
  res.status(200).json(result)
}

const getHtmlScoresPage = async () => {
    let result
    try {
        result = await axios.get("https://www.pro-football-reference.com/years/2020/week_1.htm")
        result = result.data
    } catch (err) {
        res.status(500).json(err)
    }
    return result
}

const getDataFromHTML = html => {
    const root = HTMLParser.parse(html)
    const dataContent = root.querySelector(`div.game_summaries`)
    return getGameSummaries(dataContent)
}

const getGameSummaries = html => {
    const summary = html.querySelector(`div.game_summary`)
    const date = getDate(summary)
    const winner = getWinner(summary)
    const loser = getLoser(summary)

    const result = {date, winner, loser}
    return result
}

const getTeams = html => {
    return html.querySelector(`table.teams`)
}

const getDate = html => {
    const teams = getTeams(html)
    const date = teams.querySelector(`tbody tr.date td`).rawText
    return date
}

const getWinner = html => {
    const teams = getTeams(html)
    const winner = teams.querySelector(`tbody tr.winner td a`).rawText
    return winner
}

const getLoser = html => {
    const teams = getTeams(html)
    const loser = teams.querySelector(`tbody tr.loser td a`).rawText
    return loser
}
